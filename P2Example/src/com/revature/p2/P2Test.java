package com.revature.p2;


import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.revature.pom.EmployeeManagementPage;
import com.revature.pom.EmployeeTimeManagementPage;
import com.revature.pom.NavigationBar;
import com.revature.pom.Login;
import com.revature.pom.NewLeavePage;
import com.revature.pom.NewShiftPage;

public class P2Test {
	public WebDriver driver;
	
	
  @Test(priority =0)
  public void verifyLaunch() {
	  try {
		Thread.sleep(400);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	  Assert.assertEquals(driver.getTitle(), "");
  }
  @Test(priority =1)
  public void verifyLogin() {
	  
	  Login.user(driver).clear();
	  Login.user(driver).sendKeys("guest");
	  Login.password(driver).sendKeys("off2hand");
	  Login.login(driver).click();
	  System.out.println("before get title");
//	  System.out.println(driver.getTitle());
	  
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  while(driver.getTitle()==""){}
	  //System.out.println(driver.getTitle());
	  System.out.println("after get title");
	  Assert.assertEquals(driver.getTitle(), "Revature eHRMS Application");
  }
  
  @Test(priority=2)
  public void openEmployeeManagement()
  {
	  Login.user(driver).clear();
	  Login.user(driver).sendKeys("guest");
	  Login.password(driver).sendKeys("off2hand");
	  Login.login(driver).click();
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  NavigationBar.way1(driver).click();
	  //System.out.println(driver.findElement(By.xpath("//td[@class=\'table_header\']")).getText());
	  
	  //System.out.println(driver.findElement(By.xpath("//form[@action=\'/eHRMS/leaveRequest.do?method=list\']")));
	  Assert.assertEquals(EmployeeManagementPage.checkHeader(driver), "Leave Request");
	  		
  }
  @Test(priority=3)
  public void openEmployeeManagementway2()
  {
	  Login.user(driver).clear();
	  Login.user(driver).sendKeys("guest");
	  Login.password(driver).sendKeys("off2hand");
	  Login.login(driver).click();
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  NavigationBar.way2(driver).click();
	  //System.out.println(driver.findElement(By.xpath("//td[@class=\'table_header\']")).getText());
	  
	  //System.out.println(driver.findElement(By.xpath("//form[@action=\'/eHRMS/leaveRequest.do?method=list\']")));
	  Assert.assertEquals(EmployeeManagementPage.checkHeader(driver).getText(), "Leave Request");
	  		
  }
  @Test(priority=4)
  public void addNewLeave()
  {
	  Login.user(driver).clear();
	  Login.user(driver).sendKeys("guest");
	  Login.password(driver).sendKeys("off2hand");
	  Login.login(driver).click();
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  NavigationBar.way2(driver).click();
	  EmployeeManagementPage.newLeave(driver).click();
	  NewLeavePage.save(driver).click();
	  //System.out.println(NewLeavePage.checkHeader(driver).getText());
	  //System.out.println(driver.findElement(By.xpath("//form[@action=\'/eHRMS/leaveRequest.do?method=list\']")));
	  Assert.assertEquals(NewLeavePage.checkHeader(driver).getText(),"Leave Request data updated successfully.");
	  		
  }
  @Test(priority=5)
  public void openEmployeeTimeManagement()
  {
	  Login.user(driver).clear();
	  Login.user(driver).sendKeys("guest");
	  Login.password(driver).sendKeys("off2hand");
	  Login.login(driver).click();
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  NavigationBar.way1(driver).click();
	  
	  
	  //System.out.println(driver.findElement(By.xpath("//form[@action=\'/eHRMS/leaveRequest.do?method=list\']")));
	  Assert.assertEquals(EmployeeTimeManagementPage.checkHeader(driver).getText(), "Employee Shift Timing Update");
	  		
  }
  @Test(priority=6)
  public void openEmployeeTimeManagementway2()
  {
	  Login.user(driver).clear();
	  Login.user(driver).sendKeys("guest");
	  Login.password(driver).sendKeys("off2hand");
	  Login.login(driver).click();
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  NavigationBar.way3(driver).click();
	  
	  
	  //System.out.println(driver.findElement(By.xpath("//form[@action=\'/eHRMS/leaveRequest.do?method=list\']")));
	  Assert.assertEquals(EmployeeTimeManagementPage.checkHeader(driver).getText(), "Employee Shift Timing Update");
	  		
  }
  @Test(priority=7)
  public void addTimeSheet()
  {
	  Login.user(driver).clear();
	  Login.user(driver).sendKeys("guest");
	  Login.password(driver).sendKeys("off2hand");
	  Login.login(driver).click();
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  NavigationBar.way3(driver).click();
	  EmployeeTimeManagementPage.addShift(driver).click();
	  NewShiftPage.save(driver).click();
	  NewShiftPage.checkHeader(driver);
	  //System.out.println(driver.findElement(By.xpath("//form[@action=\'/eHRMS/leaveRequest.do?method=list\']")));
	  //Assert.assertEquals(driver.findElement(By.xpath("//form[@action=\'/eHRMS/leaveRequest.do?method=list\']")), "/eHRMS/leaveRequest.do?method=list");
	  		
  }
  
  @Test(priority=8)
  public void logout()
  {
	  Login.user(driver).clear();
	  Login.user(driver).sendKeys("guest");
	  Login.password(driver).sendKeys("off2hand");
	  Login.login(driver).click();
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  NavigationBar.logout(driver).click();
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  Assert.assertEquals(driver.getTitle(),"");
  }
  
  
  
  
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("before method");
	  File file = new File("C:/JEE/Selenium-WebDriver/selenium-2.53.1/chromedriver.exe");
	  System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
	  driver = new ChromeDriver();
	  driver.get("http://192.168.60.11:7001/eHRMS/login.jsp");
  }

  @AfterMethod
  public void afterMethod() {
	  driver.quit();
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("before test");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("after test");
  }

}
