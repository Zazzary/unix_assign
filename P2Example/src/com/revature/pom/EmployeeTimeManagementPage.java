package com.revature.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EmployeeTimeManagementPage {
	
	private static WebElement element = null;
	
	
	public static WebElement checkHeader(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//td[@class=\'table_header\']"));
		return element;
	}

	public static WebElement addShift(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//a[@href=\'/eHRMS/employeesShift.do?method=add']"));
		return element;
	}
}
