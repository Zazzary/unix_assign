package com.revature.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NewShiftPage {
	
	private static WebElement element = null;
	
	public static WebElement save(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//input[@name=\'save\']"));
		return element;
	}
	public static WebElement checkHeader(WebDriver driver)
	{
		element = driver.findElement(By.className("msg_bg"));
		return element;
	}

}
