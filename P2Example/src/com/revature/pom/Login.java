package com.revature.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login {
	
	private static WebElement element = null;
	
	public static WebElement user(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//input[@name=\'j_username\']"));
		return element;
	}
	
	public static WebElement password(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//input[@name=\'j_password\']"));
		return element;
	}
	public static WebElement login(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//input[@value=\'Login\']"));
		return element;
	}
	
	
}
