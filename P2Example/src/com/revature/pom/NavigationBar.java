package com.revature.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NavigationBar {

	private static WebElement element = null;
	
	public static WebElement way1(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//a[@href=\'#\']"));
		return element;
	}
	public static WebElement way2(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//a[@href=\'/eHRMS/leaveRequest.do?method=list\']"));
		return element;
	}
	public static WebElement way3(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//a[@href=\'/eHRMS/employeesShift.do?method=list']"));
		return element;
	}
	public static WebElement logout(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//a[@href=\'/eHRMS/logout.do\']"));
		return element;
	}
}
