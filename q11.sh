#!/usr/bin/ksh

echo -n "Enter Math score: "
read math
echo -n "Enter English score: "
read english
echo -n "Ente Science score: "
read science

avg=`expr $math + $english + $science`
avg=`expr $avg / 3`

echo Average = $avg
if [ $avg -ge 90 ];
then echo Grade A
elif [ $avg -ge 80 ];
then echo Grade B
elif [ $avg -ge 70 ];
then echo Grade C
elif [ $avg -ge 60 ];
then echo Grade D
else
	echo Grade F
fi
