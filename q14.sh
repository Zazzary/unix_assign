#!/usr/bin/ksh
len=0
i=1
echo -n "Enter a String: "
read str
len=`echo $str | wc -c`
len=`expr $len - 1`
halfLen=`expr $len / 2`

echo String entered $str

while [ $i -le $halfLen ]
do
c1=`echo $str|cut -c$i`
c2=`echo $str|cut -c$len`

if [ $c1 != $c2 ] ; then
echo String was not saved
exit
fi
i=`expr $i + 1`
len=`expr $len - 1`
done

printf $str"\n" >> palindrome.txt

echo String was saved to palindrome.txt
