package pomsample;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FlightFinder {
	
	private static WebElement element = null;
	private static Props prop = new Props("locator.properties");
	
	public static WebElement oneWay(WebDriver driver)
	{
		Properties props = prop.getProps();
		element = driver.findElement(By.xpath(props.getProperty("oneway")));
		return element;
	}
	
	public static WebElement roundTrip(WebDriver driver)
	{
		Properties props = prop.getProps();
		element = driver.findElement(By.xpath(props.getProperty("roundtrip")));
		return element;
	}
	public static WebElement continueBtn(WebDriver driver)
	{
		Properties props = prop.getProps();
		element = driver.findElement(By.xpath(props.getProperty("findFlights")));
		return element;
	}
/*	public static WebElement oneWay(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//input[@value=\'oneway\']"));
		return element;
	}*/
	
	
}
