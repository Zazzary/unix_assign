package pomsample;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class TestClass {

	private static WebDriver driver=null;
		
	public static void main(String[] args) {
		File file = new File("C:/JEE/Selenium-WebDriver/selenium-2.53.1/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		driver = new InternetExplorerDriver();
		driver.get("http://newtours.demoaut.com/");
		
		LoginPage.user(driver).sendKeys("zach1");
		LoginPage.password(driver).sendKeys("zach1");
		LoginPage.login(driver).click();
		
		FlightFinder.oneWay(driver).click();
		FlightFinder.continueBtn(driver).click();
		driver.quit();
		
	}

}
