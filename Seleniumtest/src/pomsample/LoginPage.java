package pomsample;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	private static WebElement element = null;
	
	public static WebElement user(WebDriver driver)
	{
		element = driver.findElement(By.name("userName"));
		return element;
	}
	public static WebElement password(WebDriver driver)
	{
		element = driver.findElement(By.name("password"));
		return element;
	}
	public static WebElement login(WebDriver driver)
	{
		element = driver.findElement(By.name("login"));
		return element;
	}
}
