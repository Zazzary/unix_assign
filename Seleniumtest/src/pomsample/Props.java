package pomsample;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Props {
	
	static Properties prop;
	
	public Props(String filename)
	{
		FileInputStream file;
		try {
			file = new FileInputStream(filename);
			prop.load(file);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
	}
	public Properties getProps()
	{return prop;}
}
