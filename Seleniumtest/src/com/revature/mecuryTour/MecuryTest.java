package com.revature.mecuryTour;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MecuryTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File file = new File("C:/JEE/Selenium-WebDriver/selenium-2.53.1/ChromeDriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		WebDriver driver = new ChromeDriver();
		Properties prop =new Properties();
		try {
			FileInputStream file1 = new FileInputStream("locator.properties");
			prop.load(file1);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		driver.get("http://newtours.demoaut.com");
		//driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/a")).click();
		//driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td[2]/input")).sendKeys("test");
		login(driver,prop);
		
		System.out.println("The title is correct and this is the right page " + verify(driver,prop));
		
		flight(driver,prop);
		//driver.wait(300);
		Navigate(driver,prop);
		///html/body/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/form/table/tbody/tr/td/input
		driver.quit();
		
		try {
			prop.list(System.out);
			System.out.println("before xml is updated");
			FileOutputStream file2 = new FileOutputStream("locator.xml");
			prop.storeToXML(file2, "stored to XML");
			FileInputStream file3 = new FileInputStream("locator.xml");
			while (file3.available() > 0)
				{
				   System.out.print("" + (char) file3.read());
				}
			FileInputStream file4 = new FileInputStream("locator.xml");
			System.out.println("before loadfromXML");
			prop.loadFromXML(file4);
			prop.list(System.out);
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		///html/body/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/a
	}
	
	public static void login(WebDriver driver,Properties prop)
	{
		driver.findElement(By.name(prop.getProperty("username"))).sendKeys("zach1");
		driver.findElement(By.xpath(prop.getProperty("password"))).sendKeys("zach1");
		driver.findElement(By.name(prop.getProperty("login"))).click();
	}
	
	public static Boolean verify(WebDriver driver,Properties prop)
	{
		while(!driver.getTitle().equals("Find a Flight: Mercury Tours:")){}
		if(driver.findElement(By.xpath(prop.getProperty("sign-out"))).getText().equals("SIGN-OFF"))
		{return true;}
		return false;
	}
	
	public static void flight(WebDriver driver,Properties prop)
	{
		driver.findElement(By.xpath(prop.getProperty("findFlights"))).click();
		driver.findElement(By.name(prop.getProperty("reserveflight"))).click();
		driver.findElement(By.name(prop.getProperty("firstname"))).sendKeys("the");
		driver.findElement(By.name(prop.getProperty("lastname"))).sendKeys("best");
		driver.findElement(By.name(prop.getProperty("creditnum"))).sendKeys("1234567891234567");
		driver.findElement(By.name(prop.getProperty("buyflight"))).click();
		
	}
	public static void Navigate(WebDriver driver, Properties prop)
	{
		
		driver.findElement(By.xpath(prop.getProperty("support"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the support button works "+driver.getTitle().equals("Support: Mercury Tours"));
		driver.findElement(By.xpath(prop.getProperty("contact"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the contact button works "+driver.getTitle().equals("Contact: Mercury Tours"));
		driver.findElement(By.xpath(prop.getProperty("flight"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the flight button works "+driver.getTitle().equals("Find a Flight: Mercury Tours:"));
		driver.findElement(By.xpath(prop.getProperty("itinerary"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the itinerary button works "+driver.getTitle().equals("Select a Flight: Mercury Tours"));
		driver.findElement(By.xpath(prop.getProperty("profile"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the profile button works "+driver.getTitle().equals("Register: Mercury Tours"));
		driver.findElement(By.xpath(prop.getProperty("hotel"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the hotel button works "+driver.getTitle().equals("Hotel: Mercury Tours:"));
		driver.findElement(By.xpath(prop.getProperty("carrentals"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the Car Rentals button works "+driver.getTitle().equals("Car Rentals: Mercury Tours:"));
		driver.findElement(By.xpath(prop.getProperty("cruises"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the cruises button works "+driver.getTitle().equals("Cruises: Mercury Tours"));
		driver.findElement(By.xpath(prop.getProperty("destinations"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the destinations button works "+driver.getTitle().equals("Destinations: Mercury Tours:"));
		driver.findElement(By.xpath(prop.getProperty("vacations"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the vacations button works "+driver.getTitle().equals("Vacations: Mercury Tours:"));
		// home logs you out
		driver.findElement(By.xpath(prop.getProperty("home"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the home button works "+driver.getTitle().equals("Welcome: Mercury Tours"));
		driver.findElement(By.xpath(prop.getProperty("register"))).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("the register button works "+driver.getTitle().equals("Register: Mercury Tours"));
		
		
	}
}
