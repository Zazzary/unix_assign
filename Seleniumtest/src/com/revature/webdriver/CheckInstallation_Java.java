package com.revature.webdriver;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckInstallation_Java {

	public static void main(String[] args) {
		File file = new File("C:/JEE/Selenium-WebDriver/selenium-2.53.1/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		WebDriver driver = new ChromeDriver();
		////*[@id="lst-ib"]
		driver.get("http://google.com");
		driver.findElement(By.xpath("//*[@id=\"lst-ib\"]")).sendKeys("SDET webdriver");
		driver.findElement(By.xpath("//*[@id=\"lst-ib\"]")).sendKeys(Keys.RETURN);
		
		
		driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
		String title = driver.getTitle();
		String newt = driver.getTitle();
		while(title.equals(newt))
		{
			newt=driver.getTitle();
		}
		System.out.println(newt);
		
		driver.quit();
		
	}

}
