package com.revature.webdriver;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class HelloWorldIE {
	public static void main(String[] args){
		
		File file = new File("C:/JEE/Selenium-WebDriver/selenium-2.53.1/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		WebDriver driver = new InternetExplorerDriver();
		
		driver.get("http://google.com");
		driver.findElement(By.xpath("//*[@id=\'lst-ib\']")).sendKeys("SDET webdriver");
		//driver.wait(300);
		
		
		driver.quit();
		
	}
}
